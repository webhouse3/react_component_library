import { ThemeProvider as MUIThemeProvider } from '@mui/material/styles'
import { ThemeProvider } from 'emotion-theming'
import { muiTheme } from '../src/styles/baseTheme'

import '@fontsource/open-sans'

export const decorators = [
  Story => (
    <MUIThemeProvider theme={muiTheme}>
      <ThemeProvider theme={muiTheme}>{Story()}</ThemeProvider>
    </MUIThemeProvider>
  ),
]

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}
