module.exports = {
  presets: ['@babel/preset-react', '@babel/preset-env'],
  plugins: [
    'babel-plugin-macros',
    '@babel/plugin-proposal-object-rest-spread',
    '@babel/plugin-proposal-class-properties',
    '@babel/plugin-proposal-export-default-from',
  ],
  sourceType: 'unambiguous',
}
