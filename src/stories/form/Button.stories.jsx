import React from 'react'
import { Button } from 'components/form'
import { Typography } from '@mui/material'

export default {
  title: 'Form/Button',
  component: Button,
  argTypes: {},
}

const Template = args => <Button {...args} />

export const Primary = Template.bind({})
Primary.argTypes = {
  endIcon: { control: { disable: true } },
  startIcon: { control: { disable: true } },
}
Primary.args = {
  color: 'primary',
  children: 'Primary Button',
  onClick: () => ({}),
}

export const Secondary = Template.bind({})
Secondary.argTypes = {
  endIcon: { control: { disable: true } },
  startIcon: { control: { disable: true } },
}
Secondary.args = {
  color: 'secondary',
  children: 'Secondary Button',
  onClick: () => ({}),
}

export const TextButton = Template.bind({})
TextButton.argTypes = {
  endIcon: { control: { disable: true } },
  startIcon: { control: { disable: true } },
}
TextButton.args = {
  color: 'inherit',
  variant: 'text',
  children: <Typography variant='body1'>Forget Password?</Typography>,
  onClick: () => ({}),
}

export const Cancel = Template.bind({})
Cancel.argTypes = {
  endIcon: { control: { disable: true } },
  startIcon: { control: { disable: true } },
}
Cancel.args = {
  color: 'inherit',
  children: 'Cancel',
  onClick: () => ({}),
}
